const NodeCache = require('node-cache');
const myCache = new NodeCache({ stdTTL: 6000, checkperiod: 120 });

module.exports = {
	SetCache: (data) => {
		try {
			let success = myCache.set(`${data.key}`, data.user);
			if (success) {
				return {
					success: 'success',
				};
			} else {
				return {
					failed: 'failed',
				};
			}
		} catch (err) {
			throw err;
		}
    },
    HasCacheData : (key) => {
        try {
            exists = myCache.has( `${key}` );
        if(exists) {
            return exists
        }else {
            return exists
        }
        }catch(err) {
            return {
                err
            }
        }
    },

	GetCache: (key) => {
		try {
			value = myCache.get(`${key}`);
			if (value == undefined) {
				return {
					failed: 'no data found for the key',
				};
			} else {
				return value;
			}
		} catch (err) {
			return {
				err: err,
			};
		}
	},
};
