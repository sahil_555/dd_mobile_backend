const mongoose =  require('mongoose')
const Schema = mongoose.Schema


const BlockSchema = new Schema({
    user:{
        type:String,
        required:true
    },
    blockedUser:[
        {
            type:String,
            required:true
        }
    ],
},{timestamps:true})

module.exports = Blocked = mongoose.model('block',BlockSchema) 

