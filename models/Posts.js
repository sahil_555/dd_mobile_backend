const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema(
	{
		user: {
			type: String,
			required: true,
		},
		userName: {
			type: String,
			required: true,
		},
		Avatar: {
			type: String,
			required: true,
		},
		postType: {
			type: String,
			required: true,
		},
		postUrl: {
			type: String,
			required: true,
		},
		postlevel: {
			type: Number,
			default: 1,
		},
		postRange: {
			type: Number,
			default: 1,
		},
		music: {
			type: String,
		},
		musicName: {
			type: String,
		},
		title: {
			type: String,
		},
		caption: {
			type: String,
		},
		tag: {
			type: String,
			required: true,
		},
		thumbnail: {
			type: String,
			required: true,
		},
		count: {
			type: Number,
			required: true,
		},
		swags: [
			{
				type: String,
			},
		],
		cringes: [
			{
				type: String,
			},
		],
		Plays: [
			{
				user: {
					type: String,
				},
				count: {
					type: Number,
					max:5
				},
			},
		],
		Promoted: {
			type: Number,
		},
		postAverage: {
			type: Number,
		},
		Comments: [
			{
				user: {
					type: Schema.Types.ObjectId,
					ref: 'User',
				},
				body: {
					type: String,
					required: true,
				},
				name: {
					type: String,
					required: true,
				},
				avatar: {
					type: String,
					required: true,
				},
			},
		],
		lastPromoted: {
			type: Date,
			default: Date.now(),
		},
	},
	{ timestamps: true }
);

module.exports = Posts = mongoose.model('posts', PostSchema);
