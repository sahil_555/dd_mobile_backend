const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SuggestionSchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	users: [],
	posts: [
		{
			type: String,
		},
	],
});

module.exports = Suggestions = mongoose.model('Suggestion', SuggestionSchema);
