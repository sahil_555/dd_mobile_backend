const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProfileLevelSchema = new Schema(
	{
		userId: {
			type: String,
			required: true,
		},
		creatorLevel: {
			type: String,
			required: true,
		},
		numberLevel: {
			type: Number,
			required: true,
		},
		chainedAudienceRange: {
			type: Number,
			required: true,
		},
		range: {
			type: Number,
			required: true,
		},
		swipeup: {
			type: Boolean,
			default: false,
		},
		shoutout: {
			type: Boolean,
			default: false,
		},
		creatorsupport: {
			type: Boolean,
			default: false,
		},
		AdvertisePlans: {
			type: Boolean,
			default: false,
		},
		customAdvertisePlan: {
			type: Boolean,
			default: false,
		},
		subscriptionPlans: {
			type: Boolean,
			default: false,
		},
		
	},
	{ timestamps: true }
);

module.exports = ProfileLevel = mongoose.model('ProfileLevel', ProfileLevelSchema);
