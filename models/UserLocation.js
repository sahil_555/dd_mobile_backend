const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pointSchema = new Schema({});

const LocationSchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	location: [
		{
			type: {
				type: String, // Don't do `{ location: { type: String } }`
				enum: ['Point'], // 'location.type' must be 'Point'
				required: true,
			},
			coordinates: {
				type: [Number],
				required: true,
			},
		},
	],
});

module.exports = Location = mongoose.model('Location', LocationSchema);
