const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSuggestionSchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	posts: [
		{
			type: String,
		},
	],
});

module.exports = PostSuggestion = mongoose.model('PostSuggestion', PostSuggestionSchema);
