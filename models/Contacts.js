const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContactSchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	Contacts: [
		{
			name: {
				type: String,
			},
			phoneNumbers: [
				{
					type: String,
				},
			],
			emailAddress: [
				{
					type: String,
				},
			],
			postalAddress:[ {
				type: String,
			}],
			givenName: {
				type: String,
			},
			familyName: {
				type: String,
			},
			middleName: {
				type: String,
			},
			jobTitle: {
				type: String,
			},
		},
	],
});

module.exports = Contact = mongoose.model('Contact', ContactSchema);
