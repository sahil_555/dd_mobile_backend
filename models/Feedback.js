const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FeedbackSchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	feedback: {
		type: String,
		required: true,
	},
});

module.exports = Feedback = mongoose.model('feedback', FeedbackSchema);
