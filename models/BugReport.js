const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BugReportSchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	detail: {
		type: String,
		required: true,
	},
});

module.exports = BugReport = mongoose.model('bugReport', BugReportSchema);
