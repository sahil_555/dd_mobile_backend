const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PincodeSchema = new Schema({
	StateName: {
		type: String,
	},
	DistrictName: {
		type: String,
	},
	subdistname: {
		type: String,
	},
	villagename: {
		type: String,
	},
	locality_detail1: {
		type: String,
	},
	locality_detail2: {
		type: String,
	},
	locality_detail3: {
		type: String,
	},
	officeName: {
		type: String,
	},
	Pincode: {
		type: Number,
		required:true
	},
});

module.exports = Pincode = mongoose.model('pincodes', PincodeSchema);
