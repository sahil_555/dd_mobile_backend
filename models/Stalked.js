const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StalkedSchema = new Schema(
	{
		user: {
			type: String,
			rewquired: true,
		},
		Stalked: [
			{
				user: {
					type: Schema.Types.ObjectId,
					ref: 'users',
				},
				count: {
					type: Number,
					required: true,
				},
			},
		],
	},
	{ timestamps: true }
);

module.exports = Stalked = mongoose.model('stalked', StalkedSchema);
