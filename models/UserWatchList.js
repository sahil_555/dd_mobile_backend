const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserWatchListSchema = new Schema(
	{
		user: {
			type: String,
			required: true,
		},
		posts: [
			{
				type: String,
				required: true,
			},
		],
	},
	{ timestamps: true }
);

module.exports = UserWatchList = mongoose.model('userwatchlist', UserWatchListSchema);
