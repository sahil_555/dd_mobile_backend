const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	secondUser: {
		type: String,
		// required: true,
	},
	postId: {
		type: String,
		// required:true
	},
	userAvatar: {
		type: String,
		required: true,
	},
	userName: {
		type: String,
		required: true,
	},
	title: {
		type: String,
		required: true,
	},
	body: {
		type: String,
		required: true,
	},
	image: {
		type: String,
		required: true,
	},
	postThumbnail: {
		type: String,
	},
},{timestamps:true});

module.exports = Notification = mongoose.model('notification', NotificationSchema);
