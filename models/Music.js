const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MusicSchema = new Schema({
	user: {
		type: String,
	},
	musicName:{
		type:String
	},
	musicUrl: {
		type: String,
	},
	category: {
		type: String,
	},
});

module.exports = Music = mongoose.model('music', MusicSchema);
