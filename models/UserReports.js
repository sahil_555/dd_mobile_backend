const mongoose = require('mongoose')
const Schema = mongoose.Schema


const UserReportSchema = new Schema({
    user:{
        type:String,
        required:true
    },
    ReportType:{
        type:String,
        required:true
    },
    postId:{
        type:String,
        required:true
    },
 

},{timestamps:true})


module.exports = UserReport = mongoose.model('userreport',UserReportSchema)