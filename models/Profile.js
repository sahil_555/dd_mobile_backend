const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProfileSchema = new Schema(
	{
		user: {
			type: Schema.Types.ObjectId,
			ref: 'User',
		},
		bio: {
			type: String,
			// required: true
		},
		contactNumber: {
			type: Number,
			required: true,
		},
		pincode: {
			type: Number,
			required: true,
		},
		instagramHandle: {
			type: String,
		},
		youtubeHandle: {
			type: String,
		},
		Chained: [
			{
				user: {
					type: Schema.Types.ObjectId,
					ref: 'users',
				},
			},
		],
		Chaining: [
			{
				user: {
					type: Schema.Types.ObjectId,
					ref: 'users',
				},
			},
		],
		Plays: {
			type: Number,
		},
		totalSwags: {
			type: Number,
			default: 0,
		},

		totalCringes: {
			type: Number,
			default: 0,
		},
		profileAverage:{
			type:Number
		},
		totalViews: {
			type: Number,
		},
		totalKing: {
			type: Number,
		},
		totalPosts: {
			type: Number,
			default:0
		},
		promotedUser: {
			type: Number,
			default: 0,
		},
		gender: {
			type: String,
			required: true,
		},
		active: {
			type: Boolean,
			default: true,
		},
		suspended: {
			type: Boolean,
			default: false,
		},
		chaingothrough: {
			type: Boolean,
			default: true,
		},
		homegothrough: {
			type: Boolean,
			default: true,
		},
		exploregothrough: {
			type: Boolean,
			default: true,
		},
		profilegothrough: {
			type: Boolean,
			default: true,
		},
	},
	{ timestamps: true }
);
module.exports = Profile = mongoose.model('Profile', ProfileSchema);
