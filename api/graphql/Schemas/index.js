const { buildSchema } = require('graphql');

module.exports = `

scalar Upload

type User  {
    _id:ID!
    name:String!
    familyName:String!
    avatar:String!
    email:String!
    new:Boolean!
    fcmToken:String!
    createdAt:String!
    updatedAt:String!
}

type Profile {
    _id:ID!
    user:User!
    bio:String
    contactNumber:String!
    pincode:Int!
    instagramHandle:String
    youtubeHandle:String
    Chained:[User]
    Chaining:[User]
    Plays:Int
    totalSwags:Int
    totalCringes:Int
    totalViews:Int
    totalKing:Int
    gender:Boolean!
    active:Boolean
    suspended:Boolean
    chaingothrough:Boolean
    exploregothrough:Boolean
    homegothrough:Boolean
    profilegothrough:Boolean
    createdAt:String!
    updatedAt:String!
}

type Contacts {
    name:String
    PhoneNumbers:[Float]
    emailAddress:[String]
    postalAddress:String
    givenName:String
    familyName:String
    middleName:String
    jobTitle:String
}

type Play {
    user:ID,
    count:Int
}


type Posts {
    _id:ID
    user:ID
    userName:String!
    Avatar:String!
    postType:String!
    postUrl:String!
    music:ID
    musicName:String
    title:String
    caption:String
    tag:String!
    thumbnail:String
    swags:[String]
    cringes:[String]
    Plays:[Play]
    count:Int
    externalLink:String
    Comments:[Comment]
    postRange:Int
    createdAt:String!
    updatedAt:String!

}

type Notification {
    user:ID!
    secondUser:ID
    postId:ID
    userAvatar:String
    userName:String
    title:String
    body:String,
    image:String
    postThumbnail:String
    createdAt:String!
    updatedAt:String!
}

type Music {
    user:ID
    musicUrl:String
}


type Comment {
    user: ID!
    body: String,
    name: String,
    avatar: String,
}


type File {
    filename: String!
    mimetype: String!
    encoding: String!
}

type location {
    type:String!,coordinates:[Float!]
}

type Location {
    user:User!,
    location:[location!]
}

type Suggestion {
    user:User!,
    users:[Location]
}

type Contact {
    user:User!
    Contacts:[Contacts]
}

type Stalked {
    user:User!
    Stalked:[User]
}


type PostCount {
    count:Int
}

type Both {
    Profile:Profile
    User:User
}

type PostsSwagged {
    postId:ID!
    swagsCount:Int
    cringeCount:Int
}


type UserReport {
    user:ID!,
    ReportType:String,
    postId:ID!
    createdAt:String!
    updatedAt:String!
}



input UserInput {
    name:String!
    familyName:String!
    avatar:String!
    email:String!
    token:String!
}

input ProfileInput {
    userID:ID!
    gender:Boolean!
    pincode:Int!
    contactNumber:Float!
}

input ContactInput {
    name:String
    PhoneNumbers:[String]
    emailAddresses:[String]
    postalAddress:[String]
    givenName:String
    familyName:String
    middleName:String
    jobTitle:String
}


type Blocked {
    user:ID
    blockedUser:[ID]
}

input ContactsInput {
    user:ID!
    Contacts:[ContactInput]
}

input userlocation {
    type:String
    coordinates:[Float!]
}

input LocationInput {
    user:ID!
    location:[userlocation]
}

input userUpdateInput {
    _id:ID!
}

input UserId {
    _id:ID!
}

input getProfileId {
    _id:ID!
}

input feedbackInput {
    _id:ID!
    feedback:String
}

input bugInput {
    _id:ID!
    title:String
    detail:String
}

input postInput {
    user:ID!
    postType:String!
    postUrl:String!
    music:String
    musicName:String
    caption:String
    tag:String
    thumbnail:String
    count:Int
    title:String
    externalLink:String
}

input updateInput {
    user:ID!
    name:String
    bio:String
    avatar:String
    instagram:String
    youtube:String
}

input swagsInput {
    currentUserID:ID!
    postID:ID!
}

input commentInput {
    postId:ID!
    user: ID!
    body: String,
    name: String,
    avatar: String,
}

input ReportInput {
    user:ID!,
    ReportType:String,
    postId:ID!,
    createdAt:String
    updatedAt:String
}



type Query {
    getUser(_id:ID!):User!
    getProfile(getProfileInput:ID!) : Profile!
    getStalked(userId:ID!) : Stalked
    getCurrentUserPosts(userID:ID!) : [Posts]
    getPostsCount(userID:ID!) : PostCount
    getRecentPost(userID:ID!) : [Posts]
    getCurrentUserChainingPosts(userID:ID!) : [Posts]
    getSinglePost(postId:ID!) : Posts!
    getCurrentUserNotification(userID:ID!) : [Notification]
    getCurrentUserBlockedList(userID:ID!) : Blocked
    getSuggestionOnLocation(suggestionId:ID!,page:Int!) : [User]
    getSuggestionOnPincode(suggestionId:ID!,page:Int!) : [User]
    getSuggestionsOnBasedOnEveryAttribute(currentUserID:ID!,page:Int!,tag:String) : [Posts]
    getSuggestedPostForExplore(currentUserID:ID!,page:Int!) : [Posts]
    getUserSuggestionOnLocationUserInkm(UserID:ID!,page:Int!,km:Int!) : [User]
    suggestedRisingStar(suggestionId:ID!,page:Int!) : [User]
    suggestedRisingStarPosts(suggestionId:ID!,page:Int!) : [Posts]
    upgradePostLevel(postID:ID!) : [Posts]
    upgradeProfileLevel(userID:ID!) :   Profile
    getAllPostSuggestions(userID:ID!,page:Int!) : [Posts]
    getAllUserSuggestions(userID:ID!,page:Int!) : [User]
    getAllBegRiseSuggestions(userID:ID!,page:Int!) : [Posts]

}

type Mutation {
    createUser(userInput:UserInput) : User!
    createProfile(profileInput:ProfileInput) : Profile!
    updateNewUser(userUpdateInput:ID!) : User
    updateUser(updateInput:updateInput) : Both
    updateUserToken(token:String!,userId:ID!) : User
    createContacts(contactsInput:ContactsInput) : User
    saveLocationOfUser(locationInput:LocationInput) : User
    Chain(userOne:ID!,userTwo:ID!) : Profile
    ChainDemo(userID:ID!) : Profile
    HomeDemo(userID:ID!) : Profile
    profileDemo(userID:ID!) : Profile
    searchUser(searchText:String!,userID:ID!) : [User]
    searchPosts(searchText:String!) : [Posts]
    createStalked(stalkedUser:ID!,currentUser:ID!) : Stalked
    createPosts(postsInput:postInput) : Posts
    Swagged(swagsInput:swagsInput) : PostsSwagged
    Cringe(cringeInput:swagsInput) : PostsSwagged
    UnSwagged(unswagInput:swagsInput) : PostsSwagged
    UnCringe(unswagInput:swagsInput) : PostsSwagged
    Play(postId:ID!,playUserID:ID!) : Posts
    Comments(CommentInput:commentInput) : Posts
    ReportPost(reportInput:ReportInput) : UserReport
    DeletePost(postID:ID!) : Posts
    Feedback(userID:ID!,feedback:String!) : User
    BugReport(userId:ID!,detail:String!) : User
    BlockUser(userId:ID!,blockUserId:ID!) : Blocked
    UnBlockUser(userId:ID!,UnblockUserId:ID!) : Blocked
    watchList(userId:ID!,postID:ID!) : [Posts]
    Upload:Upload
} 


`;
//

// contactNumber:Float!
// pincode:Int!
// gender:Boolean!

// schema {
//     query:RootQuery,
//     mutation:RootMutation
// }
