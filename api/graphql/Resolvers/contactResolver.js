const Contact = require('../../../models/Contacts');

module.exports = {
	createContacts: async (args) => {
		try {
			const contact = await Contact.findOne({ user: args.contactsInput.user });
			if (contact) {
				return;
			} else {
				const newContact = new Contact({
					user: args.contactsInput.user,
					// Contacts: args.contactsInput.Contacts,
					Contacts: [],

				});
				const result = await newContact.save();
				if (result) {
					return;
				}
			}
		} catch (err) {
            console.log(err)
			throw err;
		}
	},
};
