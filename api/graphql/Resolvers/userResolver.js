const Profile = require('../../../models/Profile');
const User = require('../../../models/User');
const Posts = require('../../../models/Posts');
const transFormUser = (user) => {
	return {
		...user._doc,
		name: user.name,
		_id: user.id,
		createdAt: new Date(user._doc.createdAt).toISOString(),
		updatedAt: new Date(user._doc.updatedAt).toISOString(),
	};
};

module.exports = {
	getUser: async (args) => {
		try {
			let result = await User.findById(args._id).lean();
			if (result) {
				return {
					...result,
					name: result.name,
					_id: result._id,
					createdAt: new Date(result.createdAt).toISOString(),
					updatedAt: new Date(result.updatedAt).toISOString(),
				};
			} else {
				return;
			}
		} catch (err) {
			throw err;
		}
	},
	createUser: async (args) => {
		console.log(args);
		try {
			const existingUser = await User.findOne({ email: args.userInput.email });
			if (existingUser) {
				return { ...existingUser._doc, _id: existingUser.id };
			}
			let data = args.userInput;
			const user = new User({
				name: data.name,
				email: data.email,
				familyName: data.familyName,
				avatar: data.avatar,
				fcmToken: data.token,
				new: true,
			});

			const result = await user.save();

			return { ...result._doc, _id: result.id };
		} catch (err) {
			throw err;
		}
	},
	updateNewUser: async (args) => {
		try {
			let result = await User.updateOne(
				{ _id: args.userUpdateInput },
				{
					$set: { new: false },
				}
			);
			if (result) {
				let UserData = await User.findById(args.userUpdateInput);
				return {
					...UserData._doc,
					name: UserData.name,
					_id: UserData.id,
					createdAt: new Date(UserData.createdAt).toISOString(),
					updatedAt: new Date(UserData.updatedAt).toISOString(),
				};
			}
		} catch (err) {
			throw err;
		}
	},
	updateUserToken: async (args) => {
		try {
			let result = await User.findOneAndUpdate(
				{ _id: args.userId },
				{
					$set: {
						fcmToken: args.token,
					},
				}
			);
			if (result) {
				let UserData = await User.findById(args.userId);
				return {
					...UserData._doc,
					name: UserData.name,
					_id: UserData.id,
					createdAt: new Date(UserData.createdAt).toISOString(),
					updatedAt: new Date(UserData.updatedAt).toISOString(),
				};
			}
		} catch (err) {
			throw err;
		}
	},
	searchUser: async (args) => {
		try {
			const searchResult = await User.find({
				name: { $regex: '^' + args.searchText, $options: 'i' },
			}).lean();

			let finalResults = searchResult.filter((value) => {
				return value._id.toString() !== args.userID.toString();
			});

			return finalResults;
		} catch (err) {
			throw err;
		}
	},

	searchPosts: async (args) => {
		console.log(args);
		try {
			const searchResultPosts = await Posts.find({
				title: { $regex: '^' + args.searchText, $options: 'i' },
			}).lean();
			console.log(searchResultPosts);
			return searchResultPosts;
		} catch (err) {
			console.log(err);
		}
	},

	updateUser: async (args) => {
		let userResult = await User.findOneAndUpdate(
			{ _id: args.updateInput.user },
			{
				$set: {
					name: args.updateInput.name,
					avatar: args.updateInput.avatar,
				},
			}
		);

		if (userResult) {
			console.log(userResult, 'userresult');

			let profileResult = await Profile.findOneAndUpdate(
				{ user: args.updateInput.user },
				{
					$set: {
						bio: args.updateInput.bio,
						instagramHandle: args.updateInput.instagram,
						youtubeHandle: args.updateInput.youtube,
					},
				}
			);
			if (profileResult) {
				console.log(profileResult, 'profile');
				return {
					Profile: profileResult,
					User: userResult,
				};
			} else {
				throw 'no profile found';
			}
		} else {
			throw 'no user found';
		}
	},
};
