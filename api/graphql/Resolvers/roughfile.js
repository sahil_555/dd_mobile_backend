const Location = require('../../../models/UserLocation');
const Suggestions = require('../../../models/UserSuggestion');
const User = require('../../../models/User');
const Profile = require('../../../models/Profile');
const UserLocation = require('../../../models/UserLocation');
const Posts = require('../../../models/Posts');

const userFinder = () => {};

const getSuggestionIDonLocation = async (userId, pages) => {
	try {
		const resPerPage = 20; // results per page
		const page = pages || 1; // Page
		const userLocation = await UserLocation.findOne({ user: userId });

		let currentUserCordinates;
		userLocation.location.filter((data) => {
			currentUserCordinates = data.coordinates;
		});

		const nearByUserLocation = await UserLocation.find({
			location: { $geoWithin: { $centerSphere: [currentUserCordinates, 10 / 6378.1] } },
		})
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		let userID = [];

		nearByUserLocation.map((user) => {
			if (user.user !== userId) {
				userID.push(user.user);
			}
		});
		return userID;
	} catch (err) {
		console.log(err);
	}
};

const getSuggestionIDonLocationRange20km = async (userId, pages) => {
	try {
		const resPerPage = 20; // results per page
		const page = pages || 1; // Page
		const userLocation = await UserLocation.findOne({ user: userId });

		let currentUserCordinates;
		userLocation.location.filter((data) => {
			currentUserCordinates = data.coordinates;
		});

		const nearByUserLocation = await UserLocation.find({
			location: { $geoWithin: { $centerSphere: [currentUserCordinates, 20 / 6378.1] } },
		})
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		let userID = [];

		nearByUserLocation.map((user) => {
			if (user.user !== userId) {
				userID.push(user.user);
			}
		});
		return userID;
	} catch (err) {
		console.log(err);
	}
};

const getSuggestionIDOnPincode = async (userId, pages) => {
	const resPerPage = 20; // results per page
	const page = pages || 1; // Page
	const profile = await Profile.findOne({ user: userId });
	if (profile) {
		const profiles = await Profile.find({ pincode: profile.pincode });
		const ids = profiles.map((profile) => {
			return profile.user;
		});
		return ids;
	} else {
	}
};

module.exports = {
	getSuggestionsPincode: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page
		const profile = await Profile.findOne({ user: args.suggestionId });
		if (profile) {
			const profiles = await Profile.find({ pincode: profile.pincode });
			const ids = profiles.map((profile) => {
				return profile.user;
			});
			const userProfile = await User.find({ _id: { $in: ids } })
				.skip(resPerPage * page - resPerPage)
				.limit(resPerPage);
			console.log(userProfile,'profile')
			return userProfile;
		} else {
			const users = await User.find({}).limit(10);
			return users;
		}
	},

	// getSuggestionsLocation: async (args) => {
	// 	const resPerPage = 20; // results per page
	// 	const page = args.page || 1; // Page
	// 	const userLocation = await UserLocation.findOne({ user: args.suggestionId });

	// 	let currentUserCordinates;
	// 	userLocation.location.filter((data) => {
	// 		currentUserCordinates = data.coordinates;
	// 	});

	// 	const nearByUserLocation = await UserLocation.find({
	// 		location: { $geoWithin: { $centerSphere: [currentUserCordinates, 20 / 6378.1] } },
	// 	})
	// 		.skip(resPerPage * page - resPerPage)
	// 		.limit(resPerPage);

	// 	let userID = [];

	// 	nearByUserLocation.map((user) => {
	// 		if (user.user !== args.suggestionId) {
	// 			userID.push(user.user);
	// 		}
	// 	});

	// 	console.log(userID,'userrrr')
	// 	const users = await User.find({ _id: { $in: userID } })
	// 		.skip(resPerPage * page - resPerPage)
	// 		.limit(resPerPage);

	// 	return users;
	// },

	// getSuggestedPostForExplore: async (args) => {
	// 	const resPerPage = 20; // results per page
	// 	const page = args.page || 1; // Page
	// 	const pincodeBasedUser = await getSuggestionIDOnPincode(args.currentUserID, args.page);
	// 	const locationBasedUser = await getSuggestionIDonLocation(args.currentUserID, args.page);
	// 	const locationBasedUser20km = await getSuggestionIDonLocationRange20km(
	// 		args.currentUserID,
	// 		args.page
	// 	);
	// 	const locationBasedPosts = await Posts.find({
	// 		user: { $in: locationBasedUser },
	// 	})
	// 		.skip(resPerPage * page - resPerPage)
	// 		.sort({ _id: -1 })
	// 		.limit(resPerPage);
	// 	const locationBasedPosts20km = await Posts.find({
	// 		user: { $in: locationBasedUser20km },
	// 	})
	// 		.skip(resPerPage * page - resPerPage)
	// 		.sort({ _id: -1 })
	// 		.limit(resPerPage);
	// 	const PincodeBasedPosts = await Posts.find({
	// 		user: { $in: pincodeBasedUser },
	// 	})
	// 		.skip(resPerPage * page - resPerPage)
	// 		.sort({ _id: -1 })
	// 		.limit(resPerPage);

	// 	return [...locationBasedPosts, ...locationBasedPosts20km, ...PincodeBasedPosts];
	// },

	getSuggestionOnTagsPosts: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page
		const posts = await Posts.find({ tag: args.tag })
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);
		if (posts) {
			return posts;
		} else {
			throw new Error('no posts found');
		}
	},

	getSuggestionsOnBasedOnEveryAttribute: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page
		let tagsBasedPosts;
		const pincodeBasedUser = await getSuggestionIDOnPincode(args.currentUserID, args.page);
		const locationBasedUser = await getSuggestionIDonLocation(args.currentUserID, args.page);

		const PincodeBasedPosts = await Posts.find({
			$and: [{ user: { $in: pincodeBasedUser } }, { tag: args.tag }],
		})
			.skip(resPerPage * page - resPerPage)
			.sort({ _id: -1 })
			.limit(resPerPage);
		const locationBasedPosts = await Posts.find({
			$and: [{ user: { $in: locationBasedUser } }, { tag: args.tag }],
		})
			.skip(resPerPage * page - resPerPage)
			.sort({ _id: -1 })
			.limit(resPerPage);

		const finalArray = [...PincodeBasedPosts, ...locationBasedPosts];
		return finalArray;
	},

	getSuggestionsBasedOnNearbyPosts: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page

		const locationBasedUser = await getSuggestionIDonLocation(args.currentUserID, args.page);

		const locationBasedPosts = await Posts.find({ user: { $in: locationBasedUser } })
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		return locationBasedPosts;
	},
};
