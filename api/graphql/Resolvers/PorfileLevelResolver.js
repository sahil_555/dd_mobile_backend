const ProfileLevel = require('../../../models/ProfileLevel');
const { BeginierLevel } = require('./Helper/profileLevelVar');
const UserLocation = require('../../../models/UserLocation');
const Profile = require('../../../models/Profile');
const mongoose = require('mongoose');

const getUserLocationRange = async (cordinates, range, userId) => {
	const nearByUserLocation = await UserLocation.find({
		location: { $geoWithin: { $centerSphere: [cordinates, range / 6378.1] } },
	}).lean();
	let userID = [];

	nearByUserLocation.map((user) => {
		if (user.user !== userId) {
			userID.push(user.user);
		}
	});
	return userID;
};

const getProfileChaining = async (userid) => {
	let profile = await Profile.findOne({ user: userid }).lean().select({ Chained: 1 });
	console.log(profile);
	let ChainedIds = profile.Chained.map((value) => {
		return value.user.toString();
	});
	return ChainedIds;
};

const promoteProfileToSuggestion = async (suggestedUserIds, userId, postID, range) => {
	// compare all the ids  with chained of current profile

	//and then addcurrent user id to suggestion of array of ids

	let suggestedFinal = await Suggestions.updateMany(
		{ user: { $in: finalSuggesteduserIds } },
		{
			$addToSet: {
				posts: postID,
			},
		}
	).lean();

	if (suggestedFinal) {
		//then update the profile level on the basis of range decided
		updateProfile(postID, range, finalSuggesteduserIds.length);
	}
};

const updateProfile = async (postID, range, audience) => {
	let posts = await Posts.findByIdAndUpdate(postID, {
		$set: {
			Promoted: audience,
			postRange: range,
			lastPromoted: Date.now(),
		},
	});
	console.log(posts, 'posts');
};

module.exports = {
	upgradeProfileLevel: async (args) => {
		//gather all the necessary requirement from the db
		const profileLevel = await ProfileLevel.findOne({ userId: args.userID })
			.lean()
			.select({ creatorLevel: 1, numberLevel: 1, range: 1 });
		const profileLocation = await UserLocation.find({ user: args.userID })
			.lean()
			.select({ location: 1 });
		const profile = await Profile.findOne({ user: args.userID })
			.lean()
			.select({ totalSwags: 1, totalCringes: 1, totalViews: 1 });

		let currentUserCordinates;
		let location;
		profileLocation.map((value) => {
			location = value.location;
		});

		location.map((value) => {
			currentUserCordinates = value.coordinates;
		});

		console.log(profileLevel, ';event;');
		console.log(profileLocation, 'locationnnn');
		console.log(profile, 'profile');
		console.log(currentUserCordinates, 'cordinates');

		// check for the levels of the creator

		if (profileLevel.creatorLevel === BeginierLevel) {
			// gather all the ids according to the creator same level range

			let allUserIds = await getUserLocationRange(
				currentUserCordinates,
				profileLevel.range,
				args.userID
			);

			let allprofileWithsamePromotedUser = await Profile.find({
				$and: [{ user: { $in: allUserIds } }, { promotedUser: profile.promotedUser }],
			});

			//get all the profile with same creator level
			const similarLevelProfileIds = await ProfileLevel.find({
				$and: [
					{ userId: { $in: allprofileWithsamePromotedUser } },
					{ creatorLevel: profileLevel.creatorLevel },
				],
			})
				.lean()
				.select({ userId: 1 });

			//create array of user ids of the profile
			let allProfileIDs = similarLevelProfileIds.map((value) => {
				return mongoose.Types.ObjectId(value.userId);
			});

			//push current user id in the range

			allProfileIDs.push(mongoose.Types.ObjectId(args.userID));

			//now calculate on the average score for ranking of profile
			const scores = await Profile.aggregate([
				{ $match: { user: { $in: allProfileIDs } } },
				{
					$project: {
						// _id: 1,
						user: 1,
						profileAverage: {
							$divide: [
								{
									$divide: [
										{
											$add: [
												{ $subtract: ['$totalSwags', '$totalCringes'] },
												'$totalViews',
											],
										},
										'$totalPosts',
									],
								},
								'$promotedUser',
							],
						},
					},
				},
				{ $sort: { profileAverage: -1 } },
			]);
			if (scores) {
				//slice out 70% of profile
				console.log(scores,'score')
				const finalPromotedPosts = scores.slice(0, 0.7 * scores.length).map((value) => {
					return value.user.toString();
				});

				//check weather slice out result consist of current profile

				if (finalPromotedPosts.indexOf(args.userID.toString()) >= 0) {
					//get all the profile and in the next level range and suggest them this user
					console.log('me')
					let allUserIds = await getUserLocationRange(
						currentUserCordinates,
						5,
						args.userID
					);

					console.log(allUserIds,'idsss')

					//update the suggestions of final user ids

					//call the function promoteprofiletosuggestion
				}
			}
		}
	},
};
