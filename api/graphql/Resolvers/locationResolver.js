const User = require('../../../models/User');
const Location = require('../../../models/UserLocation');
const Suggestions = require('../../../models/Suggestions');

const createSuggestion = async (id, args) => {
	try {
		let nearBy = await Location.find({
			location: {
				$geoWithin: {
					$centerSphere: [args, 10 / 6378.1],
				},
			},
		});
		if (nearBy) {
			const user = await Suggestions.findById(id);
			let userIDs = [];

			nearBy.map((user) => {
				if (user.user.toString() !== id.toString()) {
					userIDs.push(user.user);
				}
			});

			if (!user) {
				let newSuggestion = new Suggestions({
					user: id,
					users: userIDs,
				});

				let suggestionResult = await newSuggestion.save();
			} else {
				return;
			}
		}
	} catch (err) {
		throw err;
	}
};

module.exports = {
	saveLocationOfUser: async (args) => {
		console.log(args, 'locatuion');
		const user = await User.findById(args.locationInput.user);
		try {
			const location = await Location.findOne({ user: args.locationInput.user });
			if (location) {
				createSuggestion(args.locationInput.user, location.location[0].coordinates);
				return {
					...user._doc,
					user: user,
				};
			} else {
				const newUserLocation = new Location({
					user: args.locationInput.user,
					location: args.locationInput.location,
				});
				const result = await newUserLocation.save();
				if (result) {
					createSuggestion(args.locationInput.user, result.location[0].coordinates);
					return {
						...user._doc,
						user: user,
					};
				}
			}
		} catch (err) {
			console.log(err);
		}
	},
};
