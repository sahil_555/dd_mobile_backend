const Notification = require('../../../models/Notification');

module.exports = {
	getCurrentUserNotification: async (args) => {
		const result = await Notification.find({ user: args.userID });
		if (result) {
			return result;
		} else {
			return;
		}
	},
};
