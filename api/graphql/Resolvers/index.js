const contactResolver = require('./contactResolver');
const ProfileResolver = require('./profileResolver');
const UserResolver = require('./userResolver');
const locationResolver = require('./locationResolver');
const SuggestionResolver = require('./SuggestionResolver');
const ServiceResolver = require('./ServiceResolver');
const PostResolver = require('./PostResolver');
const { GraphQLUpload } = require('graphql-upload');
const NotificationRessolver = require('./NotificationRessolver');
const Profile = require('../../../models/Profile');
const PostLevelResolver = require('./PostLevelResolver');
const PorfileLevelResolver = require('./PorfileLevelResolver');
// module.exports = {
// 	...UserResolver,
// 	...ProfileResolver,
// 	...contactResolver,
// 	...locationResolver,
// 	...SuggestionResolver,
// 	...ServiceResolver,
// };

module.exports = resolvers = {
	Query: {
		getUser: (_, args) => UserResolver.getUser(args),
		getProfile: (_, args) => ProfileResolver.getProfile(args),
		getStalked: (_, args) => ProfileResolver.getStalked(args),
		getCurrentUserPosts: (_, args) => PostResolver.getCurrentUserPosts(args),
		getPostsCount: (_, args) => PostResolver.getPostsCount(args),
		getRecentPost: (_, args) => PostResolver.getRecentPost(args),
		getCurrentUserChainingPosts: (_, args) => PostResolver.getCurrentUserChainingPosts(args),
		getSinglePost: (_, args) => PostResolver.getSinglePost(args),
		getCurrentUserNotification: (_, args) =>
			NotificationRessolver.getCurrentUserNotification(args),
		getCurrentUserBlockedList: (_, args) => ProfileResolver.getCurrentUserBlockedList(args),
		getSuggestionOnLocation: (_, args) => SuggestionResolver.getSuggestionOnLocation(args),
		getSuggestionOnPincode: (_, args) => SuggestionResolver.getSuggestionOnPincode(args),
		getSuggestionsOnBasedOnEveryAttribute: (_, args) =>
			SuggestionResolver.getSuggestionsOnBasedOnEveryAttribute(args),
		getSuggestedPostForExplore: (_, args) =>
			SuggestionResolver.getSuggestedPostForExplore(args),
		getUserSuggestionOnLocationUserInkm: (_, args) =>
			SuggestionResolver.getUserSuggestionOnLocationUserInRange(args),
		suggestedRisingStar: (_, args) => SuggestionResolver.suggestedRisingStar(args),
		suggestedRisingStarPosts: (_, args) => SuggestionResolver.suggestedRisingStarPosts(args),
		upgradePostLevel: (_, args) => PostLevelResolver.upgradePostLevel(args),
		getAllPostSuggestions: (_, args) => SuggestionResolver.getAllPostSuggestions(args),
		getAllUserSuggestions: (_, args) => SuggestionResolver.getAllUserSuggestions(args),
		getAllBegRiseSuggestions: (_, args) => SuggestionResolver.getAllBegRiseSuggestions(args),
		upgradeProfileLevel:(_,args) => PorfileLevelResolver.upgradeProfileLevel(args)
	},
	Mutation: {
		createUser: (_, args) => UserResolver.createUser(args),
		createProfile: (_, args) => ProfileResolver.createProfile(args),
		updateNewUser: (_, args) => UserResolver.updateNewUser(args),
		updateUser: (_, args) => UserResolver.updateUser(args),
		updateUserToken: (_, args) => UserResolver.updateUserToken(args),
		createContacts: (_, args) => contactResolver.createContacts(args),
		saveLocationOfUser: (_, args) => locationResolver.saveLocationOfUser(args),
		Chain: (_, args) => ProfileResolver.Chain(args),
		ChainDemo: (_, args) => ProfileResolver.ChainDemo(args),
		HomeDemo: (_, args) => ProfileResolver.HomeDemo(args),
		profileDemo: (_, args) => ProfileResolver.profileDemo(args),
		searchUser: (_, args) => UserResolver.searchUser(args),
		searchPosts: (_, args) => UserResolver.searchPosts(args),
		createStalked: (_, args) => ProfileResolver.createStalked(args),
		createPosts: (_, args) => PostResolver.createPosts(args),
		Swagged: (_, args) => PostResolver.Swagged(args),
		UnSwagged: (_, args) => PostResolver.UnSwagged(args),
		Cringe: (_, args) => PostResolver.Cringe(args),
		UnCringe: (_, args) => PostResolver.UnCringe(args),
		Play: (_, args) => PostResolver.Plays(args),
		Comments: (_, args) => PostResolver.Comments(args),
		ReportPost: (_, args) => PostResolver.reportPost(args),
		DeletePost: (_, args) => PostResolver.deletePost(args),
		Feedback: (_, args) => ServiceResolver.setFeedback(args),
		BugReport: (_, args) => ServiceResolver.setBugReport(args),
		BlockUser: (_, args) => ProfileResolver.BlockUser(args),
		UnBlockUser: (_, args) => ProfileResolver.UnBlockUser(args),
		Upload: GraphQLUpload,
		// createPosts:
	},
};
