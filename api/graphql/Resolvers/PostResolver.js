//tasks to do

// 1. logic on delete posts

const Posts = require('../../../models/Posts');
const { getUser } = require('./userResolver');

const User = require('../../../models/User');
const Profile = require('../../../models/Profile');
const Music = require('../../../models/Music');
const Notification = require('../../../models/Notification');
const helper = require('./Helper/NotificationService');
const { errorName } = require('../../../constants');
const UserReports = require('../../../models/UserReports');
const UserLocation = require('../../../models/UserLocation');
const ProfileLevel = require('../../../models/ProfileLevel');
const PostsSuggestions = require('../../../models/PostsSuggestions');
const Suggestions = require('../../../models/Suggestions');

//function for user location
const getUserLocationRange = async (cordinates, range, userId) => {
	const nearByUserLocation = await UserLocation.find({
		location: { $geoWithin: { $centerSphere: [cordinates, range / 6378.1] } },
	});
	let userID = [];

	nearByUserLocation.map((user) => {
		if (user.user !== userId) {
			userID.push(user.user);
		}
	});
	return userID;
};

const postPromotion = async (postID, id) => {
	const profileLevel = await ProfileLevel.findOne({ userId: id });
	const currentProfileLocation = await UserLocation.findOne({ user: id });
	const profile = await Profile.findOne({ user: id });

	let chainingId = profile.Chaining.map((item) => {
		return item.user;
	});

	let currentUserCordinates;
	currentProfileLocation.location.filter((data) => {
		currentUserCordinates = data.coordinates;
	});


	console.log(profileLevel.creatorLevel === 'Rising Star 1','lelelelelelelelelelelelelelelel')

	if (profileLevel.creatorLevel === 'Beginier') {
		let users = await getUserLocationRange(currentUserCordinates, profileLevel.range, id);

		if (users.length > 0) {
			let finalSuggesteduserIds = users.filter((value) => {
				if (!chainingId.includes(value)) {
					return value;
				}
			});
			let suggestedFinal = await Suggestions.updateMany(
				{ user: { $in: finalSuggesteduserIds } },
				{
					$addToSet: {
						posts: postID,
					},
				}
			);
			console.log(suggestedFinal, 'finalssss');
			if (suggestedFinal) {
				let audiencelength = finalSuggesteduserIds.length;

				let posts = await Posts.findByIdAndUpdate(postID, {
					$set: {
						Promoted: audiencelength,
						lastPromoted: Date.now(),
					},
				});

				const profileResult = await Profile.findOneAndUpdate(
					{ user: id },
					{
						$set: {
							promotedUser: profile.promotedUser + audiencelength,
							totalPosts: profile.totalPosts + 1,
						},
					}
				);
			}
			return suggestedFinal;
		}
	} else if (profileLevel.creatorLevel === 'Rising Star 1') {
		let users = await getUserLocationRange(currentUserCordinates, profileLevel.range, id);

		if (users.length > 0) {
			let finalSuggesteduserIds = users.filter((value) => {
				if (!chainingId.includes(value)) {
					return value;
				}
			});
			let suggestedFinal = await Suggestions.updateMany(
				{ user: { $in: finalSuggesteduserIds } },
				{
					$addToSet: {
						posts: postID,
					},
				}
			);
			console.log(suggestedFinal, 'finalssss');
			if (suggestedFinal) {
				let audiencelength = finalSuggesteduserIds.length;

				let posts = await Posts.findByIdAndUpdate(postID, {
					$set: {
						postRange:profileLevel.range,
						Promoted: audiencelength,
						lastPromoted: Date.now(),
					},
				});

				const profileResult = await Profile.findOneAndUpdate(
					{ user: id },
					{
						$set: {
							promotedUser: profile.promotedUser + audiencelength,
							totalPosts: profile.totalPosts + 1,
						},
					}
				);
			}
			return suggestedFinal;
		}
	} else if (profileLevel.creatorLevel === 'Rising Star 2') {
	} else if (profileLevel.creatorLevel === 'KQ') {
	} else {
		return null;
	}
};

module.exports = {
	getRecentPost: async (args) => {
		try {
			let result = await Posts.find({ user: args.userID }).sort({ _id: -1 }).limit(1);
			return result;
		} catch (err) {
			throw err;
		}
	},
	getPostsCount: async (args) => {
		try {
			const result = await Posts.find({ user: args.userID });

			if (result.length > 0) {
				return {
					count: result.length,
				};
			} else {
				return {
					count: 0,
				};
			}
		} catch (err) {
			throw 'something went wrong';
		}
	},
	createPosts: async (args) => {
		console.log(args, 'here here');
		try {
			const user = await User.findById(args.postsInput.user);

			let music;
			if (args.postsInput.music !== '') {
				const musicdata = new Music({
					user: args.postsInput.user,
					musicUrl: args.postsInput.music,
					musicName: args.postInput.musicName
				});

				music = await musicdata.save();
			}
			let newPost = new Posts({
				user: args.postsInput.user,
				userName: user.name,
				Avatar: user.avatar,
				title: args.postsInput.title,
				postType: args.postsInput.postType,
				postUrl: args.postsInput.postUrl,
				music: music ? music._id : '',
				musicName: args.postsInput.musicName,
				caption: args.postsInput.caption,
				tag: args.postsInput.tag,
				thumbnail: args.postsInput.thumbnail,
				count: args.postsInput.count,
			});

			let result = await newPost.save();
			if (result) {
				console.log(result);
				postPromotion(result.id, args.postsInput.user);
				return {
					...result._doc,
				};
			}
		} catch (err) {
			console.log(err);
		}
	},
	getSinglePost: async (args) => {
		try {
			const Post = await Posts.findById(args.postId);
			console.log(Post);
			if (Post) {
				return {
					...Post._doc,
					_id: Post.id,
				};
			} else {
				return;
			}
		} catch (err) {
			console.log(err);
		}
	},
	getCurrentUserPosts: async (args) => {
		console.log(args, 'postssssssss');
		try {
			const result = await Posts.find({ user: args.userID }).lean()
			const Users = await User.findById(args.userID);
			if (result) {
				console.log(result, 'result.....');
				return result;
			} else {
				throw 'No Posts from this user';
			}
		} catch (err) {
			throw err;
		}
	},
	getCurrentUserChainingPosts: async (args) => {
		try {
			console.log(args, 'args...................');
			const profile = await Profile.findOne({ user: args.userID });
			if (profile) {
				let chaining = profile.Chaining.map((value) => {
					return value.user;
				});
				console.log(chaining, 'chaining');
				const postResult = await Posts.find({
					user: { $in: chaining },
				});
				let swagsID;

				console.log(postResult, 'result');
				if (postResult) {
					// results.map(result => {
					// 	result.swags.map(user => {
					// 		console.log(user)
					// 	})
					// })
					const data = postResult.map((value) => {
						value.createdAt = new Date(value.createdAt).toISOString();
						value.updatedAt = new Date(value.updatedAt).toISOString();
						return value;
					});
					console.log(data,'datatata')
					return data;
				} else {
					throw new Error(errorName.SERVER_ERROR);
				}
			} else {
				throw new Error(errorName.SERVER_ERROR);
			}
		} catch (err) {
			console.log(err);
			throw new Error(errorName.SERVER_ERROR);
		}
	},
	Swagged: async (args) => {
		try {
			const UserOne = await User.findById(args.swagsInput.currentUserID);
			const Post = await Posts.findById(args.swagsInput.postID);
			const PostPorfile = await Profile.findOne({ user: Post.user });
			const PostUser = await User.findById(Post.user);
			console.log(PostUser, 'user.................');
			if (Post) {
				if (
					Post.swags.filter((id) => id.toString() === args.swagsInput.currentUserID)
						.length > 0
				) {
					return {
						postId: Post._id,
						swagsCount: Post.swags.length,
						cringeCount: Post.cringes.length,
					};
				} else {
					if (
						Post.cringes.filter((id) => id.toString() === args.swagsInput.currentUserID)
							.length > 0
					) {
						const removeIndex = Post.cringes
							.map((item) => item.toString())
							.indexOf(args.swagsInput.currentUserID);

						// Splice out of array
						Post.cringes.splice(removeIndex, 1);
						Post.swags.unshift(args.swagsInput.currentUserID);
						let postsResult = await Post.save();
						if (postsResult) {
							const PostProfileResult = await Profile.findOneAndUpdate(
								{ user: Post.user },
								{
									$set: {
										totalSwags: PostPorfile.totalSwags + 1,
										totalCringe:
											PostPorfile.totalCringe > 0
												? PostPorfile.totalCringe - 1
												: 0,
									},
								}
							);
							if (PostProfileResult) {
								const newNotification = new Notification({
									user: Post.user,
									secondUser: args.swagsInput.currentUserID,
									userName: UserOne.name,
									postId: args.swagsInput.postID,
									userAvatar: UserOne.avatar,
									title: 'DashDub',
									body: `${UserOne.name} swagged on your ${postResults.postType}`,
									image: UserOne.avatar,
									postThumbnail: Post.thumbnail,
								});
								const success = await newNotification.save();
								if (success) {
									helper.sendNotification(
										PostUser.fcmToken,
										'DashDub',
										`Your Post has been Swagged`,
										UserOne.avatar
									);
								}
							}
							return {
								postId: postsResult._id,
								swagsCount: postsResult.swags.length,
								cringeCount: postsResult.cringes.length,
							};
						} else {
							return;
						}
					} else {
						Post.swags.unshift(args.swagsInput.currentUserID);
						let posts = await Post.save();
						if (posts) {
							const PostProfileResult = await Profile.findOneAndUpdate(
								{ user: Post.user },
								{
									$set: {
										totalSwags: PostPorfile.totalSwags + 1,
										totalCringe:
											PostPorfile.totalCringe > 0
												? PostPorfile.totalCringe - 1
												: 0,
									},
								}
							);

							if (PostProfileResult) {
								const newNotification = new Notification({
									user: Post.user,
									secondUser: args.swagsInput.currentUserID,
									userName: UserOne.name,
									postId: args.swagsInput.postID,
									userAvatar: UserOne.avatar,
									title: 'DashDub',
									body: `Your post has been swagged`,
									image: UserOne.avatar,
									postThumbnail: Post.thumbnail,
								});
								const success = await newNotification.save();
								console.log(PostUser.fcmToken);
								if (success) {
									helper.sendNotification(
										PostUser.fcmToken,
										'DashDub',
										`Your Post has been Swagged`,
										UserOne.avatar
									);
								}
							}
							return {
								postId: posts._id,
								swagsCount: posts.swags.length,
								cringeCount: posts.cringes.length,
							};
						}
					}
				}
			}
		} catch (err) {
			throw err;
		}
	},
	UnSwagged: async (args) => {
		try {
			const user = await User.findById(args.unswagInput.currentUserID);

			if (user) {
				const Post = await Posts.findById(args.unswagInput.postID);
				const PostPorfile = await Profile.findOne({ user: Post.user });
				if (
					Post.swags.filter((like) => like.toString() === args.unswagInput.currentUserID)
						.length === 0
				) {
					return {
						postId: Post._id,
						swagsCount: Post.swags.length,
						cringeCount: Post.cringes.length,
					};
				} else {
					const removeIndex = Post.swags
						.map((item) => item.toString())
						.indexOf(args.unswagInput.currentUserID);

					// Splice out of array
					Post.swags.splice(removeIndex, 1);

					// Save
					const post2 = await Post.save();
					if (post2) {
						const PostProfileResult = await Profile.findOneAndUpdate(
							{ user: Post.user },
							{
								$set: {
									totalSwags:
										PostPorfile.totalSwags > 0
											? PostPorfile.totalSwags - 1
											: PostPorfile.totalSwags,
								},
							}
						);
						return {
							postId: post2._id,
							swagsCount: post2.swags.length,
							cringeCount: post2.cringes.length,
						};
					}
				}
			}
		} catch (err) {
			console.log(err);
		}
	},
	Cringe: async (args) => {
		try {
			const UserOne = await User.findById(args.cringeInput.currentUserID);
			const Post = await Posts.findById(args.cringeInput.postID);
			const PostPorfile = await Profile.findOne({ user: Post.user });
			const PostUser = await User.findById(Post.user);
			console.log(PostUser, 'user.................');
			if (Post) {
				if (
					Post.cringes.filter((id) => id.toString() === args.cringeInput.currentUserID)
						.length > 0
				) {
					return {
						postId: Post._id,
						swagsCount: Post.swags.length,
						cringeCount: Post.cringes.length,
					};
				} else {
					if (
						Post.swags.filter((id) => id.toString() === args.cringeInput.currentUserID)
							.length > 0
					) {
						const removeIndex = Post.swags
							.map((item) => item.toString())
							.indexOf(args.cringeInput.currentUserID);

						// Splice out of array
						Post.swags.splice(removeIndex, 1);
						Post.cringes.unshift(args.cringeInput.currentUserID);
						let postsResult = await Post.save();
						console.log(PostPorfile.totalCringes, 'totalcringes');
						if (postsResult) {
							const PostProfileResult = await Profile.findOneAndUpdate(
								{ user: Post.user },
								{
									$set: {
										totalSwags:
											PostPorfile.totalSwags > 0
												? PostPorfile.totalSwags - 1
												: 0,
										totalCringes: PostPorfile.totalCringes + 1,
									},
								}
							);

							return {
								postId: postsResult._id,
								swagsCount: postsResult.swags.length,
								cringeCount: postsResult.cringes.length,
							};
						} else {
							return;
						}
					} else {
						Post.cringes.unshift(args.cringeInput.currentUserID);
						let posts = await Post.save();
						if (posts) {
							console.log(PostPorfile.totalCringes, 'totalcringes');

							const PostProfileResult = await Profile.findOneAndUpdate(
								{ user: Post.user },
								{
									$set: {
										totalSwags:
											PostPorfile.totalSwags > 0
												? PostPorfile.totalSwags - 1
												: 0,
										totalCringes: PostPorfile.totalCringes + 1,
									},
								}
							);

							return {
								postId: posts._id,
								swagsCount: posts.swags.length,
								cringeCount: posts.cringes.length,
							};
						}
					}
				}
			}
		} catch (err) {
			throw err;
		}
	},
	UnCringe: async (args) => {
		const user = await User.findById(args.unswagInput.currentUserID);
		if (user) {
			const post = await Posts.findById(args.unswagInput.postID);
			if (
				post.cringes.filter((like) => like.toString() === args.unswagInput.currentUserID)
					.length === 0
			) {
				return {
					postId: post._id,
					swagsCount: post.swags.length,
					cringeCount: post.cringes.length,
				};
			} else {
				// Get remove index
				const removeIndex = post.cringes
					.map((item) => item.user.toString())
					.indexOf(args.unswagInput.currentUserID);

				// Splice out of array
				post.cringes.splice(removeIndex, 1);

				// Save
				const post2 = await post.save();
				let profileCringe = await Profile.findOne({ user: post.user });
				if (post2) {
					if (profileCringe.totalCringe !== 0) {
						let profile = await Profile.findOneAndUpdate(
							{
								user: post.user,
							},
							{
								$set: {
									totalCringes:
										profileCringe.totalCringes > 0
											? profileCringe.totalCringes - 1
											: profileCringe.totalCringes,
								},
							}
						);
					}
					return {
						postId: post._id,
						swagsCount: post.swags.length,
						cringeCount: post.cringes.length,
					};
				}
			}
		}
	},
	Plays: async (args) => {
		try {
			const post = await Posts.findById(args.postId);

			if (post.Plays.length === 0) {
				const play = [
					{
						user: args.playUserID,
						count: 1,
					},
				];

				let updatedPost = await Posts.findByIdAndUpdate(args.postId, {
					$set: {
						Plays: play,
					},
				});

				return updatedPost;
				// console.log(upda);
			} else {
				// check the plays if already exist and then increase the count
				// check if not already exist the add the id
				let isExist = post.Plays.map((value) => {
					if (value.user === args.playUserID.toString()) {
						value.count = value.count + 1;
					}
					return value;
				});
				console.log(isExist);
				if (isExist.length > 0) {
					let result = await post.save();
					return result;
				} else {
					const play = {
						user: args.playUserID,
						count: 1,
					};
					post.Plays.push(play);

					let result = post.save();
					return result;
				}
			}
		} catch (err) {
			throw new Error(err);
		}
	},
	watchList: async (args) => {
		try {
		} catch (err) {
			throw new Error(err);
		}
	},
	Comments: async (args) => {
		try {
			let comment = {
				body: args.CommentInput.body,
				name: args.CommentInput.name,
				avatar: args.CommentInput.avatar,
				user: args.CommentInput.user,
			};

			let post = await Posts.findById(args.CommentInput.postId);
			let user = await User.findById(args.CommentInput.user);
			const PostUser = await User.findById(post.user);
			console.log(PostUser.fcmToken, 'userrrrrrr');
			if (post) {
				post.Comments.unshift(comment);
				let posts = post.save();

				if (posts) {
					const notification = new Notification({
						user: post.user,
						secondUser: args.CommentInput.user,
						userName: user.name,
						postId: args.CommentInput.postId,
						userAvatar: user.avatar,
						title: 'DashDub',
						body: `${user.name} Commented on your post`,
						image: user.avatar,
						postThumbnail: post.thumbnail,
					});
					const notificationResult = await notification.save();
					if (notificationResult) {
						helper.sendNotification(
							PostUser.fcmToken,
							'DashDub',
							`${user.name} Commented on your post`,
							user.avatar
						);
					}
					return posts;
				}
			}
		} catch (err) {
			throw err;
		}
	},
	reportPost: async (args) => {
		try {
			const { user, ReportType, postId } = args.reportInput;
			const newReport = UserReports({
				user,
				ReportType,
				postId,
			});

			let result = await newReport.save();
			if (result) {
				return {
					...result._doc,
				};
			}
		} catch (err) {
			throw new Error(errorName.SERVER_ERROR);
		}
	},
	deletePost: async (args) => {
		console.log(args, 'delete Post...................');
		// delete posts and then cut of the swags cringes and plays from profile total swags cringes and plays
		try {
			let result = await Posts.findByIdAndDelete(args.postID);
			if (result) {
				return;
			} else {
				console.log;
			}
		} catch (err) {
			console.log(err);
		}
	},
};
