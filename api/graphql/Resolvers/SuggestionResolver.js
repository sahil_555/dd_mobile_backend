const Suggestions = require('../../../models/Suggestions');
const User = require('../../../models/User');
const Profile = require('../../../models/Profile');
const UserLocation = require('../../../models/UserLocation');
const Posts = require('../../../models/Posts');
const ProfileLevel = require('../../../models/ProfileLevel');

//helper methods

const getSuggestionIDonLocation = async (userId, pages) => {
	try {
		const resPerPage = 20; // results per page
		const page = pages || 1; // Page
		const userLocation = await UserLocation.findOne({ user: userId });

		let currentUserCordinates;
		userLocation.location.filter((data) => {
			currentUserCordinates = data.coordinates;
		});

		const nearByUserLocation = await UserLocation.find({
			location: { $geoWithin: { $centerSphere: [currentUserCordinates, 50 / 6378.1] } },
		})
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		let userID = [];

		nearByUserLocation.map((user) => {
			if (user.user !== userId) {
				userID.push(user.user);
			}
		});
		return userID;
	} catch (err) {
		console.log(err);
	}
};

const getSuggestionIDonLocationRange20km = async (userId, pages) => {
	try {
		const resPerPage = 20; // results per page
		const page = pages || 1; // Page
		const userLocation = await UserLocation.findOne({ user: userId });

		let currentUserCordinates;
		userLocation.location.filter((data) => {
			currentUserCordinates = data.coordinates;
		});

		const nearByUserLocation = await UserLocation.find({
			location: { $geoWithin: { $centerSphere: [currentUserCordinates, 20 / 6378.1] } },
		})
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		let userID = [];

		nearByUserLocation.map((user) => {
			if (user.user !== userId) {
				userID.push(user.user);
			}
		});
		return userID;
	} catch (err) {
		console.log(err);
	}
};

const getSuggestionIDonLocationRangekm = async (userId, pages, km) => {
	try {
		const resPerPage = 20; // results per page
		const page = pages || 1; // Page
		const userLocation = await UserLocation.findOne({ user: userId });

		let currentUserCordinates;
		userLocation.location.filter((data) => {
			currentUserCordinates = data.coordinates;
		});

		const nearByUserLocation = await UserLocation.find({
			location: { $geoWithin: { $centerSphere: [currentUserCordinates, km / 6378.1] } },
		})
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		let userID = [];

		nearByUserLocation.map((user) => {
			if (user.user !== userId) {
				userID.push(user.user);
			}
		});
		return userID;
	} catch (err) {
		console.log(err);
	}
};

const getSuggestionIDOnPincode = async (userId, pages) => {
	const resPerPage = 20; // results per page
	const page = pages || 1; // Page
	const profile = await Profile.findOne({ user: userId });
	if (profile) {
		const profiles = await Profile.find({ pincode: profile.pincode });
		const ids = profiles.map((profile) => {
			return profile.user;
		});
		return ids;
	} else {
	}
};

module.exports = {
	getSuggestionOnLocation: async (args) => {
		try {
			const resPerPage = 20; // results per page
			const page = args.page || 1; // Page
			range = 0;
			const userLocation = await UserLocation.findOne({ user: args.suggestionId });
			let currentUserCordinates;
			userLocation.location.filter((data) => {
				currentUserCordinates = data.coordinates;
			});

			//first step of searching in nearby Location

			const nearByUserLocation = await UserLocation.find({
				location: { $geoWithin: { $centerSphere: [currentUserCordinates, 30 / 6378.1] } },
			})
				.skip(resPerPage * page - resPerPage)
				.limit(resPerPage);

			if (nearByUserLocation.length > 0) {
				let userIDs = [];

				nearByUserLocation.map((user) => {
					if (user.user !== args.suggestionId) {
						userIDs.push(user.user);
					}
				});

				const profileLevels = await ProfileLevel.find({
					$and: [
						{ userId: { $in: userIDs } },
						{ creatorLevel: { $in: ['Beginier', 'Rising Star', 'KQ'] } },
					],
				});
				let levelUserID = [];
				profileLevels.map((user) => {
					if (user.userId !== args.suggestionId) {
						levelUserID.push(user.userId);
					}
				});
				const users = await User.find({ _id: { $in: levelUserID } })
					.skip(resPerPage * page - resPerPage)
					.limit(resPerPage);

				return users;
			}
		} catch (err) {
			console.log(err);
		}
	},
	getSuggestionOnPincode: async (args) => {
		try {
			const resPerPage = 20; // results per page
			const page = args.page || 1; // Page
			const profile = await Profile.findOne({ user: args.suggestionId });
			if (profile) {
				const profiles = await Profile.find({ pincode: profile.pincode });
				let ids = [];

				profiles.map((value) => {
					console.log(value.user !== args.suggestionId, 'conditionnnn');
					if (value.user.toString() !== args.suggestionId) {
						ids.push(value.user);
					}
				});

				console.log(ids, '/////');
				const userProfile = await User.find({ _id: { $in: ids } })
					.skip(resPerPage * page - resPerPage)
					.limit(resPerPage);
				console.log(userProfile, 'profile');
				return userProfile;
			} else {
				const users = await User.find({}).limit(10);
				return users;
			}
		} catch (err) {
			throw new Error(err);
		}
	},
	getSuggestionsOnBasedOnEveryAttribute: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page
		let tagsBasedPosts;
		const pincodeBasedUser = await getSuggestionIDOnPincode(args.currentUserID, args.page);
		const locationBasedUser = await getSuggestionIDonLocation(args.currentUserID, args.page);

		const PincodeBasedPosts = await Posts.find({
			$and: [{ user: { $in: pincodeBasedUser } }, { tag: args.tag }],
		})
			.skip(resPerPage * page - resPerPage)
			.sort({ _id: -1 })
			.limit(resPerPage);
		const locationBasedPosts = await Posts.find({
			$and: [{ user: { $in: locationBasedUser } }, { tag: args.tag }],
		})
			.skip(resPerPage * page - resPerPage)
			.sort({ _id: -1 })
			.limit(resPerPage);

		const finalArray = [...PincodeBasedPosts, ...locationBasedPosts];
		return finalArray;
	},
	getSuggestedPostForExplore: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page
		const pincodeBasedUser = await getSuggestionIDOnPincode(args.currentUserID, args.page);
		const locationBasedUser = await getSuggestionIDonLocation(args.currentUserID, args.page);
		const locationBasedUser20km = await getSuggestionIDonLocationRange20km(
			args.currentUserID,
			args.page
		);
		const locationBasedPosts = await Posts.find({
			user: { $in: locationBasedUser },
		})
			.skip(resPerPage * page - resPerPage)
			.sort({ _id: -1 })
			.limit(resPerPage);
		const locationBasedPosts20km = await Posts.find({
			user: { $in: locationBasedUser20km },
		})
			.skip(resPerPage * page - resPerPage)
			.sort({ _id: -1 })
			.limit(resPerPage);
		const PincodeBasedPosts = await Posts.find({
			user: { $in: pincodeBasedUser },
		})
			.skip(resPerPage * page - resPerPage)
			.sort({ _id: -1 })
			.limit(resPerPage);

		return [...locationBasedPosts, ...locationBasedPosts20km, ...PincodeBasedPosts];
	},

	getUserSuggestionOnLocationUserInRange: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page

		const UserLocation = await getSuggestionIDonLocationRangekm(
			args.UserID,
			args.page,
			args.km
		);

		console.log(UserLocation);
	},

	suggestedRisingStar: async (args) => {
		const resPerPage = 20; // results per page
		const page = args.page || 1; // Page
		const userLocation = await UserLocation.findOne({ user: args.suggestionId });
		const profile = await Profile.findOne({ user: args.suggestionId });

		let currentUserCordinates;
		userLocation.location.filter((data) => {
			currentUserCordinates = data.coordinates;
		});

		const nearByUserLocation = await UserLocation.find({
			location: { $geoWithin: { $centerSphere: [currentUserCordinates, 10 / 6378.1] } },
		})
			.skip(resPerPage * page - resPerPage)
			.limit(resPerPage);

		if (nearByUserLocation.length > 0) {
			let userIDs = [];

			nearByUserLocation.map((user) => {
				if (user.user !== args.suggestionId) {
					userIDs.push(user.user);
				}
			});

			const profileLevels = await ProfileLevel.find({
				$and: [
					{ userId: { $in: userIDs } },
					{ creatorLevel: { $in: ['Beginier', 'Rising Star'] } },
				],
			});
			let levelUserID = [];
			let chainingId = profile.Chaining.map((item) => {
				return item.user;
			});
			profileLevels.map((user) => {
				if (user.userId !== args.suggestionId) {
					levelUserID.push(user.userId);
				}
			});

			let ids = levelUserID.filter((item) => {
				if (!chainingId.includes(item)) {
					return item;
				}
			});

			console.log(ids);

			const users = await User.find({ _id: { $in: ids } })
				.skip(resPerPage * page - resPerPage)
				.limit(resPerPage);

			return users;
		}
	},
	suggestedRisingStarPosts: async (args) => {
		try {
			const resPerPage = 20; // results per page
			const page = args.page || 1; // Page
			const userLocation = await UserLocation.findOne({ user: args.suggestionId });
			const profile = await Profile.findOne({ user: args.suggestionId });

			let currentUserCordinates;
			userLocation.location.filter((data) => {
				currentUserCordinates = data.coordinates;
			});

			const nearByUserLocation = await UserLocation.find({
				location: { $geoWithin: { $centerSphere: [currentUserCordinates, 10 / 6378.1] } },
			})
				.skip(resPerPage * page - resPerPage)
				.limit(resPerPage);

			if (nearByUserLocation.length > 0) {
				let userIDs = [];

				nearByUserLocation.map((user) => {
					if (user.user !== args.suggestionId) {
						userIDs.push(user.user);
					}
				});

				const profileLevels = await ProfileLevel.find({
					$and: [
						{ userId: { $in: userIDs } },
						{ creatorLevel: { $in: ['Beginier', 'Rising Star'] } },
					],
				});
				let levelUserID = [];
				let chainingId = profile.Chaining.map((item) => {
					return item.user;
				});
				profileLevels.map((user) => {
					if (user.userId !== args.suggestionId) {
						levelUserID.push(user.userId);
					}
				});

				let ids = levelUserID.filter((item) => {
					if (!chainingId.includes(item)) {
						return item;
					}
				});

				console.log(ids);

				const Post = await Posts.find({ user: { $in: ids } })
					.skip(resPerPage * page - resPerPage)
					.limit(resPerPage);

				return Post;
			}
		} catch (err) {
			console.log(err);
		}
	},
	getAllPostSuggestions: async (args) => {
		try {
			const resPerPage = 20; // results per page
			const page = args.page || 1; // Page
			const userSuggestion = await Suggestions.findOne({ user: args.userID })
				.lean()
				.select({ posts: 1 });

			if (userSuggestion.posts.length > 0) {
				let posts = await Posts.aggregate([
					{ $match: { _id: { $in: userSuggestion.posts } } },
					{
						$project: {
							_id: 1,
							user: 1,
							thumbnail: 1,
							postUrl: 1,
							caption: 1,
							tag: 1,
							postType: 1,
							Avatar000: 1,
							userName: 1,
							caption: 1,
							title: 1,
							swags: 1,
							createdAt: 1,
							updatedAt: 1,
							cringes: 1,
							Plays: 1,
							Comments: 1,
							postAverage: {
								$divide: [
									{
										$add: [
											{
												$subtract: [
													{ $size: '$swags' },
													{ $size: '$cringes' },
												],
											},
											{ $size: '$Plays' },
										],
									},
									'$Promoted',
								],
							},
						},
					},
					{ $sort: { avg: -1 } },
				])
					.skip(resPerPage * page - resPerPage)
					.limit(resPerPage);
				return posts;
			} else {
				return [];
			}
		} catch (err) {
			throw new Error(err);
		}
	},
	getAllUserSuggestions: async (args) => {
		try {
			const resPerPage = 20; // results per page
			const page = args.page || 1; // Page
			const userSuggestion = await Suggestions.findOne({ user: args.userID })
				.lean()
				.select({ users: 1 });

			let users = await User.find({ _id: { $in: userSuggestion.users } })
				.lean()
				.select({ postlevel: 0, Promoted: 0 })
				.skip(resPerPage * page - resPerPage)
				.limit(resPerPage);
			return users;
		} catch (err) {
			throw new Error(err);
		}
	},

	getAllBegRiseSuggestions: async (args) => {
		try {
			const resPerPage = 20; // results per page
			const page = args.page || 1; // Page
			const userSuggestion = await Suggestions.findOne({ user: args.userID })
				.lean()
				.select({ posts: 1 });

			let posts = await Posts.find({
				$and: [
					{ _id: { $in: userSuggestion.posts } },
					{ $or: [{ postRange: 1 }, { postRange: 5 }] },
				],
			})
				.lean()
				.select({ Promoted: 0 })
				.skip(resPerPage * page - resPerPage)
				.limit(resPerPage);
			return posts;
		} catch (err) {
			throw new Error(err);
		}
	},
	getSuggestionsAccordingToRequirement: async (args) => {
		try {
		} catch (err) {
			throw new Error(err);
		}
	},
};
