const Profile = require('../../../models/Profile');
const User = require('../../../models/User');
const Stalked = require('../../../models/Stalked');
const Pincode = require('../../../models/Pincodes');
const Notification = require('../../../models/Notification');
const Block = require('../../../models/Block');
const helper = require('./Helper/NotificationService');
const { errorName } = require('../../../constants');
const ProfileLevel = require('../../../models/ProfileLevel');
const ProfileLevls = require('./Helper/ProfileLevls');

const FindUserById = async (id) => {
	const user = await User.findById(id);
	return {
		...user._doc,
	};
};

module.exports = {
	getProfile: async (args) => {
		try {
			let result = await Profile.findOne({ user: args.getProfileInput });
			let user = await User.findById(args.getProfileInput);
			let chaining = result.Chaining.map((user) => {
				return user.user;
			});
			let chained = result.Chained.map((user) => {
				return user.user;
			});

			let chainedUser = await User.find({
				_id: { $in: chaining },
			});

			let chainingUser = await User.find({
				_id: { $in: chained },
			});

			console.log(chainingUser);

			if (result) {
				return {
					...result._doc,
					_id: result.id,
					user: user,
					Chaining: chainedUser,
					Chained: chainingUser,
					createdAt: new Date(result._doc.createdAt).toISOString(),
					updatedAt: new Date(result._doc.updatedAt).toISOString(),
				};
			} else {
				console.log('no user found');
			}
		} catch (err) {
			throw err;
		}
	},
	createProfile: async (args) => {
		try {
			const { userID, gender, pincode, contactNumber } = args.profileInput;
			const existingProfile = await Profile.findOne({ user: userID });
			if (existingProfile) {
				let userDetail = await User.findById(existingProfile.user);
				return {
					...existingProfile._doc,
					_id: existingProfile.id,
					user: userDetail,
				};
			} else {
				const pincodeChecked = await Pincode.find({ pincode: pincode });
				if (pincodeChecked.length > 0) {
					const newProfile = new Profile({
						user: userID,
						gender: gender,
						pincode: pincode,
						contactNumber: contactNumber,
					});
					const result = await newProfile.save();
					if (result) {
						let userDetail = await User.findById(result.user);
						const profileLevel = new ProfileLevel({
							userId: userDetail._id,
							creatorLevel: 'Beginier',
							numberLevel: 1,
							range: 1,
							chainedAudienceRange: 0,
						});
						const level = profileLevel.save();
						if (level) {
							return {
								...result._doc,
								user: userDetail,
								_id: result.id,
								createdAt: new Date(result._doc.createdAt).toISOString(),
								updatedAt: new Date(result._doc.updatedAt).toISOString(),
							};
						} else {
							return {
								...result._doc,
								user: userDetail,
								_id: result.id,
								createdAt: new Date(result._doc.createdAt).toISOString(),
								updatedAt: new Date(result._doc.updatedAt).toISOString(),
							};
						}
					}
					console.log(pincodeChecked);
				} else {
					throw new Error(errorName.NO_PINNCODE_FOUND);
				}
			}
		} catch (err) {
			throw new Error(errorName.NO_PINNCODE_FOUND);
		}
	},
	Chain: async (args) => {
		try {
			const profileOne = await Profile.findOne({ user: args.userOne });
			const profileTwo = await Profile.findOne({ user: args.userTwo });

			if (
				profileOne.Chaining.filter((id) => id.user.toString() === args.UserTwo).length > 0
			) {
				return {
					...profileOne._doc,
				};
			} else {
				profileOne.Chaining.unshift({ user: args.userTwo });
				const profileOneResult = await profileOne.save();
				if (profileOneResult) {
					if (
						profileTwo.Chained.filter((id) => id.user.toString() === args.UserOne)
							.length > 0
					) {
						return {
							...profileTwo._doc,
						};
					} else {
						profileTwo.Chained.unshift({ user: args.userOne });
						const profileTwoResult = await profileTwo.save();
						if (profileTwoResult) {
							const userone = await User.findById(args.userOne);
							const usertwo = await User.findById(args.userTwo);
							const notification = new Notification({
								user: args.userTwo,
								secondUser: args.userOne,
								userName: userone.name,
								userAvatar: userone.avatar,
								title: 'DashDub',
								body: `You are been chained`,
								image: userone.avatar,
							});
							const success = await notification.save();
							if (success) {
								helper.sendNotification(
									usertwo.token,
									'DashDub',
									`You are been chained`,
									user.avatar
								);
							}
						}
					}
				}
			}
		} catch (err) {}
	},
	ChainDemo: async (args) => {
		const profile = await Profile.findOneAndUpdate(
			{ user: args.userID },
			{
				$set: {
					chaingothrough: false,
				},
			}
		);
		if (profile) {
			return {
				profile,
			};
		}
	},
	HomeDemo: async (args) => {
		const profile = await Profile.findOneAndUpdate(
			{ user: args.userID },
			{
				$set: {
					homegothrough: false,
				},
			}
		);
		if (profile) {
			return {
				profile,
			};
		}
	},
	profileDemo: async (args) => {
		const profile = await Profile.findOneAndUpdate(
			{ user: args.userID },
			{
				$set: {
					profilegothrough: false,
				},
			}
		);
		if (profile) {
			return {
				profile,
			};
		}
	},
	createStalked: async (args) => {
		const stalkedUser = await Stalked.findOne({ user: args.stalkedUser });

		if (stalkedUser) {
			stalkedUser.Stalked.map((user) => {
				if (user.user == args.currentUser) {
					console.log(user);
					user.count = user.count + 1;
				} else {
					stalkedUser.Stalked.push({
						user: args.currentUser,
						count: 1,
					});
				}
			});
			let result = await stalkedUser.save();
			console.log(result);
			return {
				result,
			};
		} else {
			let stalked = [];
			stalked.push({
				user: args.currentUser,
				count: 1,
			});
			const newStalked = new Stalked({
				user: args.stalkedUser,
				Stalked: stalked,
			});
			const result = await newStalked.save();
			console.log(result);
			return {
				result,
			};
		}
	},

	getStalked: async (args) => {
		try {
			let result = await Stalked.findOne({ user: args.userId });

			if (result) {
				return { ...result._doc };
			} else {
				return { result ,stalked:[]};
			}
		} catch (err) {
			throw err;
		}
	},
	BlockUser: async (args) => {
		try {
			const profileOne = await Profile.findOne({ user: args.userId });
			const profileTwo = await Profile.findOne({ user: args.blockUserId });

			const blockedList = await Block.findOne({ user: args.userId });

			if (blockedList) {
				const profileOneChainingResult = profileOne.Chaining.filter((value) => {
					return value.user === args.blockUserId;
				});

				const profileOneChainedResult = profileOne.Chained.filter((value) => {
					return value.user.toString() === args.blockUserId;
				});

				if (profileOneChainingResult) {
					const removeIndexOne = profileOne.Chaining.map((item) =>
						item.toString()
					).indexOf(args.blockUserId);

					// Splice out of array
					profileOne.Chaining.splice(removeIndexOne, 1);

					const removeIndexTwo = profileTwo.Chained.map((item) =>
						item.toString()
					).indexOf(args.userId);

					profileTwo.Chained.splice(removeIndexTwo, 1);

					const result1 = await profileOne.save();
					const result2 = await profileTwo.save();

					//yaha se start karna hai blocked list me check karna hai already hai ki nai aur uske basis pe condition karn ahai

					let blockedUserAlreadyExist = blockedList.blockedUser.filter((value) => {
						return value === args.blockUserId;
					});

					console.log(blockedUserAlreadyExist);
					if (blockedUserAlreadyExist.length > 0) {
						return {
							...blockedList._doc,
						};
					} else {
						blockedList.blockedUser.push(args.blockUserId);

						let finalresults = await blockedList.save();

						if (finalresults) {
							return {
								...finalresults._doc,
							};
						}
					}
				} else if (profileOneChainedResult) {
					const removeIndexOne = profileOne.Chained.map((item) =>
						item.toString()
					).indexOf(args.blockUserId);

					// Splice out of array
					profileOne.Chained.splice(removeIndexOne, 1);

					const removeIndexTwo = profileTwo.Chaining.map((item) =>
						item.toString()
					).indexOf(args.userId);

					profileTwo.Chaining.splice(removeIndexTwo, 1);

					const result1 = await profileOne.save();
					const result2 = await profileTwo.save();

					let block = [];
					block.push(args.blockUserId);

					const newBlock = new Block({
						user: args.userId,
						blockedUser: block,
					});

					const finalResult = await newBlock.save();
					if (finalResult) {
						return {
							...finalResult._doc,
						};
					}
				} else {
					let block = [];
					block.push(args.blockUserId);

					const newBlock = new Block({
						user: args.userId,
						blockedUser: block,
					});

					const finalResult = await newBlock.save();
					if (finalResult) {
						return {
							...finalResult._doc,
						};
					}
				}
			} else {
				// console.log(profileOne);
				const profileOneChainingResult = profileOne.Chaining.filter((value) => {
					return value.user === args.blockUserId;
				});

				const profileOneChainedResult = profileOne.Chained.filter((value) => {
					return value.user.toString() === args.blockUserId;
				});

				if (profileOneChainingResult) {
					const removeIndexOne = profileOne.Chaining.map((item) =>
						item.toString()
					).indexOf(args.blockUserId);

					// Splice out of array
					profileOne.Chaining.splice(removeIndexOne, 1);

					const removeIndexTwo = profileTwo.Chained.map((item) =>
						item.toString()
					).indexOf(args.userId);

					profileTwo.Chained.splice(removeIndexTwo, 1);

					const result1 = await profileOne.save();
					const result2 = await profileTwo.save();

					let block = [];
					block.push(args.blockUserId);

					const newBlock = new Block({
						user: args.userId,
						blockedUser: block,
					});

					const finalResult = await newBlock.save();
					if (finalResult) {
						return {
							...finalResult._doc,
						};
					}
				} else if (profileOneChainedResult) {
					const removeIndexOne = profileOne.Chained.map((item) =>
						item.toString()
					).indexOf(args.blockUserId);

					// Splice out of array
					profileOne.Chained.splice(removeIndexOne, 1);

					const removeIndexTwo = profileTwo.Chaining.map((item) =>
						item.toString()
					).indexOf(args.userId);

					profileTwo.Chaining.splice(removeIndexTwo, 1);

					const result1 = await profileOne.save();
					const result2 = await profileTwo.save();

					let block = [];
					block.push(args.blockUserId);

					const newBlock = new Block({
						user: args.userId,
						blockedUser: block,
					});

					const finalResult = await newBlock.save();
					if (finalResult) {
						return {
							...finalResult._doc,
						};
					}
				} else {
					let block = [];
					block.push(args.blockUserId);

					const newBlock = new Block({
						user: args.userId,
						blockedUser: block,
					});

					const finalResult = await newBlock.save();
					if (finalResult) {
						return {
							...finalResult._doc,
						};
					}
				}
			}
		} catch (err) {
			throw new Error(err);
		}
	},
	UnBlockUser: async (args) => {
		try {
			let blockedList = await Block.findOne({ user: args.userId });

			if (blockedList) {
				const removeIndex = blockedList.blockedUser
					.map((item) => item)
					.indexOf(args.UnblockUserId);
				blockedList.blockedUser.splice(removeIndex, 1);
				let results = await blockedList.save();
				if (results) {
					return {
						...results._doc,
					};
				}
			}
		} catch (err) {
			throw new Error(err);
		}
	},
	getCurrentUserBlockedList: async (args) => {
		let blockedList = await Block.findOne({ user: args.userID });

		if (blockedList) {
			return {
				...blockedList._doc,
			};
		}
	},
};
