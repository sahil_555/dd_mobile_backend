const admin = require('firebase-admin');

module.exports = {
	sendNotification: async (token, title, body, imageUrl) => {
		try {
			let data = await admin.messaging().send({
				token,
				notification: {
					title,
					body,
					imageUrl,
				},
			});
			console.log(data, 'notification send successfully');
			return true;
		} catch (err) {
			console.log(err);
		}
	},
};
