module.exports = PostLevel = [
	{
		level: 1,
		range: 1,
	},
	{
		level: 2,
		range: 2.5,
	},
	{
		level: 3,
		range: 5,
	},
	{
		level: 4,
		range: 10,
	},
	{
		level: 5,
		range: 15,
	},
	{
		level: 6,
		range: 25,
	},
	{
		level: 7,
		range: 50,
	},
	{
		level: 8,
		range: 75,
	},
	{
		level: 9,
		range: 100,
	},
	{
		level: 10,
		range: 150,
	},
	{
		level: 11,
		range: 175,
	},
	{
		level: 12,
		range: 'state',
	},
	{
		level: 13,
		range: 'nearby 3 states',
	},
	{
		level: 14,
		range: 'all metropolition cities',
	},
	{
		level: 15,
		range: 'National',
	},
	{
		level: 16,
		range: 'all',
        
	},
];
