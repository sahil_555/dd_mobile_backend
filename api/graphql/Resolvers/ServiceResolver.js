const Feedback = require('../../../models/Feedback');
const BugReport = require('../../../models/BugReport');

module.exports = {
	setFeedback: async (args) => {
		let newFeedback = new Feedback({
			user: args.userID,
			feedback: args.feedback,
		});

		let result = await newFeedback.save();

		if (result) {
			return;
		}
	},
	setBugReport: async (args) => {
		console.log(args,'args,bugsss')
		let newBugReport = new BugReport({
			user: args.userId,
			detail: args.detail,
		});

		let result = await newBugReport.save();
		if (result) {
			return;
		}
	},
};
