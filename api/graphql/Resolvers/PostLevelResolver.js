const Posts = require('../../../models/Posts');
const Profile = require('../../../models/Profile');
const ProfileLevel = require('../../../models/ProfileLevel');
const UserLocation = require('../../../models/UserLocation');

const getProfileChaining = async (userid) => {
	let profile = await Profile.findOne({ user: userid }).lean().select({ Chained: 1 });
	console.log(profile);
	let ChainedIds = profile.Chained.map((value) => {
		return value.user.toString();
	});
	return ChainedIds;
};

const promoteVideoToSuggestion = async (suggestedUserIds, userId, postID, range) => {
	// compare all the profiles with chaining of posted user
	let postUserChainedIds = await getProfileChaining(userId);

	// find current user profile

	let profile = await Profile.findOne({ user: userId }).lean()


	let finalSuggesteduserIds = suggestedUserIds.filter((value) => {
		if (!postUserChainedIds.includes(value.toString())) {
			return value;
		}
	});

	console.log(finalSuggesteduserIds);


	// update suggestion db with new post

	let suggestedFinal = await Suggestions.updateMany(
		{ user: { $in: finalSuggesteduserIds } },
		{
			$addToSet: {
				posts: postID,
			},
		}
	).lean();
	// if suugested final
	if (suggestedFinal) {
		updatePost(postID, range, finalSuggesteduserIds.length);

		const profileResult = await Profile.findOneAndUpdate(
			{ user: userId },
			{
				$set: {
					promotedUser: profile.promotedUser + finalSuggesteduserIds.length,
				},
			}
		);


		// update notification and send to the user that post is promoted to 5 km range
	}
};

const updatePost = async (postID, range, audience) => {
	let posts = await Posts.findByIdAndUpdate(postID, {
		$set: {
			Promoted: audience,
			postRange: range,
			lastPromoted: Date.now(),
		},
	});
	console.log(posts, 'posts');
};

const getUserLocationRange = async (cordinates, range, userId) => {
	const nearByUserLocation = await UserLocation.find({
		location: { $geoWithin: { $centerSphere: [cordinates, range / 6378.1] } },
	}).lean();
	let userID = [];

	nearByUserLocation.map((user) => {
		if (user.user !== userId) {
			userID.push(user.user);
		}
	});
	return userID;
};

module.exports = {
	upgradePostLevel: async (args) => {
		try {
			const Post = await Posts.findById(args.postID);
			// gtlobal variable for cuurent user cordinates
			let currentUserCordinates;

			// Post is availabel in our database or not
			if (Post) {
				// check for the post range
				if (Post.postRange === 1) {
					// finding profile userlocation  of postid
					const Profile = await UserLocation.findOne({ user: Post.user });
					// if we get the user location
					if (Profile) {
						console.log('well played');

						//setting current user cordinates
						Profile.location.filter((data) => {
							currentUserCordinates = data.coordinates;
						});

						//calling helper function for getting all the user in post range

						let userIDinRange = await getUserLocationRange(
							currentUserCordinates,
							Post.postRange,
							Post.user
						);

						console.log(userIDinRange, 'range');
						//get all posts
						const allPost = await Posts.find({
							$and: [
								{
									user: { $in: userIDinRange },
								},
								{
									postRange: Post.postRange,
								},
							],
						}).lean();

						console.log(allPost, 'posts');

						if (allPost.length > 0) {
							console.log('all', Post.postRange);
							let similarPostRange = [Post._id];

							// map all the post id in similarPostRange array

							allPost.map((value) => {
								similarPostRange.map(value._id);
							});

							console.log(similarPostRange);

							//check the length of similar posts array
							if (similarPostRange.length > 0) {
								// now calculate average on all the posts array
								const scores = await Posts.aggregate([
									{ $match: { _id: { $in: similarPostRange } } },
									{
										$project: {
											postAverage: {
												$divide: [
													{
														$add: [
															{
																$subtract: [
																	{ $size: '$swags' },
																	{ $size: '$cringes' },
																],
															},
															{ $size: '$Plays' },
														],
													},
													'$Promoted',
												],
											},
										},
									},
									{ $sort: { avg: -1 } },
								]);

								// now take top 50% of all the posts to promote them
								const finalPromotedPosts = scores
									.slice(0, scores.length / 2)
									.map((value) => {
										console.log(value);
										return value._id.toString();
									});

								//now check that current post id is in 50% data

								if (finalPromotedPosts.indexOf(args.postID.toString()) >= 0) {
									//if yes then we check for the profile in next creator level range
									//and find all the users

									let promotedIDs = await getUserLocationRange(
										currentUserCordinates,
										5,
										Post.user
									);

									//then we promote the post in all the  profile in next range

									if (promotedIDs.length > 0) {
										promoteVideoToSuggestion(
											promotedIDs,
											Post.user,
											Post._id,
											5
										);
									}
								}else {
									// yet to be decided
								}
							}else {
							//directly promote if no posts
							}
						} else {
							//directly promote if no posts
						}
					}
				} else if (Post.postRange === 5) {
				} else if (Post.postRange === 10) {
				} else if (Post.postRange === 15) {
				} else {
					return true;
				}
			}
		} catch (err) {
			console.log(err);
		}
	},
};
