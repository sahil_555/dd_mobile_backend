const express = require('express');
const bodyParser = require('body-parser');
const { GraphQLServer } = require('graphql-yoga');
const { buildSchema } = require('graphql');
const mongoose = require('mongoose');
const admin = require('firebase-admin');
const serviceAccount = require('./firebase.json');
const getErrorCode = require('./getErrorCode')
const app = express();

//firebase

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
});

const graphqlResolver = require('./api/graphql/Resolvers/index');
const graphqlSchema = require('./api/graphql/Schemas/index');

app.use(express.json({ limit: '50mb' }));

const { URLSearchParams } = require('url');
global.URLSearchParams = URLSearchParams;
// app.use(
// 	'/graphql',
// 	graphqlHTTP({
// 		//schemas
// 		//rootquery should have same name as resolvers
// 		//same for root mutation
// 		schema: graphqlSchema,
// 		//here we write our resolvers
// 		rootValue: graphqlResolver,
// 		graphiql: true,
// 	})
// );

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

const url = require('./config/keys');

const opts = {
	port: 5000,
	endpoint: '/graphql',
	playground: '/playground',
};

const server = new GraphQLServer({ typeDefs: graphqlSchema, resolvers: graphqlResolver });

server.start({
	// formatError: (err) => {
	// 	const error = getErrorCode(err.message)
	// 	console.log(error,'error')
	// 	return ({ message: error.message, statusCode: error.statusCode })
	//   }
},() => {

	mongoose
		.connect(url.MongoUri, {
			useUnifiedTopology: true,
			useNewUrlParser: true,
			useFindAndModify: false,
		})
		.then(() => {
			console.log('server is up and running');
		})
		.catch((err) => {
			console.log(err);
		});
});
