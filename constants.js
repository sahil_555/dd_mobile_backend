exports.errorName = {
	USER_ALREADY_EXISTS: 'USER_ALREADY_EXISTS',
	SERVER_ERROR: 'SERVER_ERROR',
    NO_PINNCODE_FOUND:'NO_PINNCODE_FOUND'
};

exports.errorType = {
	USER_ALREADY_EXISTS: {
		message: 'User is already exists.',
		statusCode: 403,
	},
	SERVER_ERROR: {
		message: 'Server error.',
		statusCode: 500,
	},
    NO_PINNCODE_FOUND :{
        message:'No Pincode found',
        statusCode:400
    }
};


//bb3c6cf2-ee59-456a-99f1-c0500251a34b